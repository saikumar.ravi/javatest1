package com.JavaTest;

public class ReverseArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 int[] arr = {11, 19, 15, 10};
	      int temp;
	      int n = arr.length;

	      for (int i = 0; i < n / 2; i++) {
	         temp = arr[i];
	         arr[i] = arr[n - i - 1];
	         arr[n - i - 1] = temp;
	      }

	      System.out.print("Reversed array: ");
	      for (int i = 0; i < n; i++) {
	         System.out.print(arr[i] + " ");
	      }
	}

}
