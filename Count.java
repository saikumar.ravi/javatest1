package com.JavaTest;

public class Count {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str = "Hello123andWelcome0";
        int characters = 0;
        int digits = 0;

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);

            if (Character.isLetter(c)) {
            	characters++;
            } else if (Character.isDigit(c)) {
            	digits++;
            }
        }

        System.out.println("Number of characters: " + characters);
        System.out.println("Number of digits: " + digits);
	}

}
